// To run this, first execute truffle console
let app = await Aplicacion.deployed();

let accounts = await web3.eth.getAccounts();

let owner = accounts[1];

let guest = accounts[2];

let owner2 = accounts[3];

let guest2 = accounts[4];

await app.addApartment("Apartamento Paris", "Buenas vistas y centrico", "Calle Sol", web3.utils.toWei("1", "Ether"), "123543", { from: owner } );

await app.addApartment("Penthouse 10", "Construido recientemente y amueblado", "Manhattan street", web3.utils.toWei("2", "Ether"), "234523", { from: owner });

await app.addApartment("Casa mata", "Excelente para 2 inquilinos", "Calle miragladiolos", web3.utils.toWei("0.5", "Ether"), "234515", { from: owner });

await app.addApartment("Piso planta 30", "Vistas increibles de la ciudad", "Calle principal", web3.utils.toWei("1.5", "Ether"), "132445", { from: owner });

await app.addApartment("Cabaña en las montañas", "Perfecto lugar para relajarse", "Avenida del parque", web3.utils.toWei("0.2", "Ether"), "864532", { from: owner2 });

await app.addApartment("Casa en la periferia", "Alejado del ruido de la ciudad", "Avenida 7", web3.utils.toWei("0.4", "Ether"), "234534", { from: owner2 });

await app.addApartment("Atico en las afueras", "Para disfrutar de una buena estancia", "Calle silencio", web3.utils.toWei("2", "Ether"), "675475", { from: owner2 });

await app.addApartment("Rascacielos 4", "Localizado en el edificio mas alto de la ciudad", "Calle golden", web3.utils.toWei("5", "Ether"), "743526", { from: owner2 });


// Booking simulation
// Auxiliars
let deposit = 6000000000000000;
let startDateUnix = 1631120400; // 8 SEPT 2021
let endDateUnix = 1631620800; // 14 SEPT 2021
let days = Math.floor((endDateUnix - startDateUnix) / (60 * 60 * 24)) + 1;

let apartment = await app.apartments(0);
let bookingPrice = parseInt(apartment.price) * days + deposit;

await app.book(0, startDateUnix, endDateUnix, { from: guest, value: bookingPrice });
