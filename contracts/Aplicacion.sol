//SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.4.21 <0.7.0;

contract Aplicacion {
    string public name;

    uint256 public aCont = 0;
    uint256 public bCont = 0;

    uint256 bookingDeposit = 6000000000000000; // Aprox 15USD

    // CONSTRUCTOR
    constructor() public {
        name = "DRenting";
    }

    // Booking state
    enum State {
        Created,
        Confirmed,
        Withdrawable,
        UserCanWithdraw,
        OwnerCanWithdraw,
        Rejected,
        Completed
    }

    // STRUCTURES
    // Apartment structure
    struct Apartment {
        uint256 id;
        address payable owner;
        address guest;
        string title;
        string description;
        string direction;
        uint256 price; // price per day
        bool available; // In order to disable booking on the appartment
        uint256[] bookingIDs; // Array to keep track of the booking IDs
        string accessCode;
        uint256 creationDate; // Timestamp to display creation date
    }

    // Structure to model a booking
    struct Booking {
        uint256 id;
        State state;
        uint256 startDate; // frontend - data
        uint256 endDate; // frontend - data
        uint256 nDays;
        uint256 total; // price total
        address user;
        uint256 apartmentId;
    }

    // ARRAYS
    // All apartments
    Apartment[] public apartments;

    // All bookings
    Booking[] public bookings;

    // MAPPINGS
    // userAddress => userApartments[]
    mapping(address => uint256[]) public userApartments;

    // userAddres => userBookings[]
    mapping(address => uint256[]) public userBookings;

    // address => booking.ID => booking.total
    mapping(address => mapping(uint256 => uint256)) pendingBookingWithdraws;

    // address => booking.ID => deposit
    mapping(address => mapping(uint256 => uint256)) pendingDepositWithdraws;

    // Events
    // Event emitted on create apartment
    event NewApartment(
        uint256 id,
        address owner,
        string title,
        string description,
        string direction,
        bool available,
        uint256 price,
        string accessCode,
        uint256 creationDate
    );

    // Event emitted on modify availability
    event ApartmentModified(address owner, uint256 id, string title);

    // Event emitted on create booking
    event NewBooking(
        uint256 id,
        State state,
        uint256 startDate,
        uint256 endDate,
        uint256 nDays,
        uint256 total,
        address user,
        uint256 apartmentId
    );

    event BookingStateChanged(
        uint256 apartmentId,
        uint256 bookingID,
        address owner,
        address guest,
        string description
    );

    // FUNCTIONS
    /**
     * Adds a new apartment to the application with availability true as default
     *
     * Conditions: The user must provide a title, a description, a direction
     *             and a price per day for the apartment
     *
     * @param title          Title of the apartment
     * @param description    Description of the apartment
     * @param direction      Location of the apartment
     * @param price          Price of the apartment per day (must be positive)
     * @param accessCode     Access code to the apartment
     */

    function addApartment(
        string memory title,
        string memory description,
        string memory direction,
        uint256 price,
        string memory accessCode
    ) public {
        require(price > 0, "price can't be negative");
        require(bytes(title).length > 0);
        require(bytes(description).length > 0);
        require(bytes(direction).length > 0);
        require(bytes(accessCode).length == 6);

        Apartment memory newApartment = Apartment(
            aCont,
            msg.sender,
            address(0),
            title,
            description,
            direction,
            price,
            true,
            new uint256[](0),
            accessCode,
            now
        );

        // Push to the general array
        apartments.push(newApartment);

        // Add the apartment ID to the user apartments array
        userApartments[msg.sender].push(aCont);

        // Emit event
        emit NewApartment(
            newApartment.id,
            msg.sender,
            newApartment.title,
            newApartment.description,
            newApartment.direction,
            newApartment.available,
            newApartment.price,
            newApartment.accessCode,
            newApartment.creationDate
        );
        aCont++;
    }

    /**
     * Modifies the availability of the apartment from true to false
     * Conditions:  This function is only called by the owner of the apartment.
     *              The apartment must be available to book.
     *              New bookings won't arrive as the apartment won't be show on UI.
     *              Existing bookings will continue as programed.
     *
     * @param aID   The ID of the apartment must be provided
     * @return      If the execution is correct returns true and emits event. returns false otherwise
     */

    function pauseBooking(uint256 aID) public returns (bool) {
        require(apartments[aID].owner == msg.sender);
        if (apartments[aID].available == true) {
            apartments[aID].available = false;
            emit ApartmentModified(
                msg.sender,
                aID,
                "Apartment no longer available to book"
            );
            return true;
        }

        return false;
    }

    /**
     * Modifies the availability of the apartment from false to true
     * Conditions:  This function is only called by the owner of the apartment.
     *              The apartment has to be unavailable to book
     *
     * @param aID   The ID of the apartment must be provided
     * @return      If the execution is correct returns true and emits event. returns false otherwise
     */

    function resumeBooking(uint256 aID) public returns (bool) {
        require(apartments[aID].owner == msg.sender);
        if (apartments[aID].available == false) {
            apartments[aID].available = true;
            emit ApartmentModified(
                msg.sender,
                aID,
                "Apartment is now available to book"
            );
            return true;
        }

        return false;
    }

    /**
     * Adds a new apartment to the application with availability true as default
     *
     * Conditions: The user must provide a title, a description, a direction
     *             and a price per day for the apartment
     *
     * @param aID            ID of the apartment
     * @param title          Title of the apartment
     * @param description    Description of the apartment
     * @param direction      Location of the apartment
     * @param price          Price of the apartment per day (must be positive)
     * @param accessCode     Access code to the apartment
     */

    function changeApartmentInfo(
        uint256 aID,
        string memory title,
        string memory description,
        string memory direction,
        uint256 price,
        string memory accessCode
    ) public {
        require(msg.sender == apartments[aID].owner);
        require(price > 0, "price can't be negative");
        require(bytes(title).length > 0);
        require(bytes(description).length > 0);
        require(bytes(direction).length > 0);
        require(bytes(accessCode).length == 6);

        uint256[] memory bookingsAux = getBookings(aID);

        Apartment memory apartmentModified = Apartment(
            aID,
            apartments[aID].owner,
            apartments[aID].guest,
            title,
            description,
            direction,
            price,
            apartments[aID].available,
            bookingsAux,
            accessCode,
            now
        );

        apartments[aID] = apartmentModified;
    }

    /**
     * The function allows users to book apartments. It'll receive funds and store them
     * until conditions for release met.
     * It registers the booking and manages payment control.
     * Conditions: Start date must be greater than the current one. End date must be greater than
     *             the start date. The owner of the property can't book his own house. The msg.value must be equal to
     *             the _total value of the booking + a deposit
     *
     * @param _id       Corresponds to the ID of the apartment
     * @param _start    Timestamp for the start day of the booking. Initially check in will be available at 17:00 pm
     * @param _end      Timestamp for the last day of the booking. Initially check out should be donde before 12:00 am
     */
    function book(
        uint256 _id,
        uint256 _start,
        uint256 _end
    ) public payable {
        //require(_start > now); ENABLE FOR PROD
        require(_end > _start);
        require(msg.sender != apartments[_id].owner);
        require(apartments[_id].available);

        // Check if book is possible
        uint256[] memory bookingsAux = getBookings(_id);

        // If there are no bookings, continue
        if (bookingsAux.length > 0) {
            // This loop may need improving in performance, as for now is O(N) in every case
            // Using a while loop we may increase performance avoiding innecesary checks
            for (uint256 i = 0; i < bookingsAux.length; i++) {
                if (bookings[bookingsAux[i]].state == State.Created) continue;
                require(
                    !isInBetween(
                        _start,
                        bookings[bookingsAux[i]].startDate,
                        bookings[bookingsAux[i]].endDate
                    )
                );
                require(
                    !isInBetween(
                        _end,
                        bookings[bookingsAux[i]].startDate,
                        bookings[bookingsAux[i]].endDate
                    )
                );
                require(
                    !isInBetween(
                        bookings[bookingsAux[i]].startDate,
                        _start,
                        _end
                    )
                );
                require(
                    !isInBetween(bookings[bookingsAux[i]].endDate, _start, _end)
                );
            }
        }

        // Get total days of the booking
        // Solidity applies floor to math operations if needed, so 3.9 days are 3 days
        // Adding 1 day extra because start hour is 17:00 and end hour is 12:00
        uint256 _nDays = ((_end - _start) / (60 * 60 * 24)) + 1;

        // Get total price of the booking + deposit
        uint256 bookingValue = (_nDays * apartments[_id].price);
        uint256 _total = bookingValue + bookingDeposit;

        // msg.value has to match the total price. the smart contract will store it.
        require(msg.value == _total);

        // Create new booking with the data received and calculated
        Booking memory newBooking = Booking(
            bCont,
            State.Created,
            _start,
            _end,
            _nDays,
            _total,
            msg.sender,
            _id
        );

        // Push the new booking to the apartment.bookingsIDs attribute
        apartments[_id].bookingIDs.push(newBooking.id);

        // Add the new booking to the array of all bookings
        bookings.push(newBooking);

        // Add the booking ID to the user bookings array
        userBookings[msg.sender].push(bCont);

        // Add the value of the booking to the owner withdrawable mapping
        pendingBookingWithdraws[apartments[_id].owner][bCont] = bookingValue;

        // Add the value of the deposit to the user withdrawable mapping
        pendingDepositWithdraws[msg.sender][bCont] = bookingDeposit;

        // Emit event
        emit NewBooking(
            newBooking.id,
            newBooking.state,
            newBooking.startDate,
            newBooking.endDate,
            newBooking.nDays,
            newBooking.total,
            newBooking.user,
            newBooking.apartmentId
        );

        bCont++;
    }

    /**
     * This functions allows the owner of the property to accept a new booking.
     * Booking state must be created.
     * The function changes the state of the booking to Confirmed.
     * This function should reject all bookings that are withing the confirmed booking dates
     * Conditions: This functions can only be called by the owner of the apartment.
     *
     * @param bID   Corresponds to the ID of the booking
     */
    function confirmBooking(uint256 bID) public {
        require(apartments[bookings[bID].apartmentId].owner == msg.sender);
        require(bookings[bID].state == State.Created);
        require(now < bookings[bID].startDate);
        require(
            bookings[bID].apartmentId ==
                apartments[bookings[bID].apartmentId].id
        );

        bookings[bID].state = State.Confirmed;

        // Get all bookings that are within the confirmed booking dates and set them to Rejected
        uint256[] memory bookingsAux = getBookings(bookings[bID].apartmentId);

        // If there are no bookings, continue
        if (bookingsAux.length > 0) {
            for (uint256 i = 0; i < bookingsAux.length; i++) {
                if (bookingsAux[i] == bID) continue;
                if (
                    isInBetween(
                        bookings[bID].startDate,
                        bookings[bookingsAux[i]].startDate,
                        bookings[bookingsAux[i]].endDate
                    )
                ) {
                    rejectBooking(bookingsAux[i]);
                } else if (
                    isInBetween(
                        bookings[bID].endDate,
                        bookings[bookingsAux[i]].startDate,
                        bookings[bookingsAux[i]].endDate
                    )
                ) {
                    rejectBooking(bookingsAux[i]);
                } else if (
                    isInBetween(
                        bookings[bookingsAux[i]].startDate,
                        bookings[bID].startDate,
                        bookings[bID].endDate
                    )
                ) {
                    rejectBooking(bookingsAux[i]);
                } else if (
                    isInBetween(
                        bookings[bookingsAux[i]].endDate,
                        bookings[bID].startDate,
                        bookings[bID].endDate
                    )
                ) {
                    rejectBooking(bookingsAux[i]);
                }
            }
        }

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been confirmed"
        );
    }

    /**
     * This functions allows the owner of the property to reject a new booking.
     * Booking state must be created.
     * The function allows the user who booked the apartment to withdraw his funds because of the rejection.
     * The function changes the state of the booking to Rejected.
     * Conditions: This functions can only be called by the owner of the apartment
     *
     * @param bID   Corresponds to the ID of the booking
     */
    function rejectBooking(uint256 bID) public {
        require(apartments[bookings[bID].apartmentId].owner == msg.sender);
        require(bookings[bID].state == State.Created);
        require(now < bookings[bID].startDate);
        require(
            bookings[bID].apartmentId ==
                apartments[bookings[bID].apartmentId].id
        );

        bookings[bID].state = State.Rejected;
        bookings[bID].startDate = 0;
        bookings[bID].endDate = 0;
        uint256 cashback = pendingBookingWithdraws[msg.sender][bID];
        pendingBookingWithdraws[msg.sender][bID] = 0;

        // Add the value of the booking to the user withdrawable mapping
        pendingDepositWithdraws[bookings[bID].user][bID] += cashback;

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been rejected"
        );
    }

    /**
     * This function allows the user to withdraw his available funds.
     * It should set the value of total to 0 to avoid double calls.
     * Conditions: This function may only be called by a user who owns a booking valid which state is Rejected/Withdrawable/UserCanWithdraw
     *
     * @param bID   Corresponds to the booking ID
     * @return      true if everything went correctly, false otherwise
     */
    function withdrawFunds(uint256 bID) public returns (bool) {
        require(bookings[bID].user == msg.sender);
        require(
            bookings[bID].state == State.Withdrawable ||
                bookings[bID].state == State.UserCanWithdraw ||
                bookings[bID].state == State.Rejected
        );

        address payable user = msg.sender;

        uint256 amount = pendingDepositWithdraws[msg.sender][bID];

        bookings[bID].state == State.Rejected ||
            bookings[bID].state == State.UserCanWithdraw
            ? bookings[bID].state = State.Completed
            : bookings[bID].state = State.OwnerCanWithdraw;

        if (amount > 0) {
            // Important to set this to 0 in order to avoid double calls
            pendingDepositWithdraws[user][bID] = 0;

            // If something fails, reset amount to give
            if (!user.send(amount)) {
                pendingDepositWithdraws[user][bID] = amount;
                return false;
            }
        } else {
            return false;
        }

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been completed"
        );

        return true;
    }

    /**
     * This function allows the owner of an apartment to withdraw his profits.
     * Conditions: This function may only be called by the owner of the apartment.
     *             The booking state should be CheckIn as the user confirmed the entry.
     *
     * @param bID   Corresponds to the booking ID.
     * @return      true if everything went correctly, false otherwise
     */
    function withdrawBooking(uint256 bID) public returns (bool) {
        require(apartments[bookings[bID].apartmentId].owner == msg.sender);
        require(
            bookings[bID].state == State.Withdrawable ||
                bookings[bID].state == State.OwnerCanWithdraw
        );

        bookings[bID].state == State.OwnerCanWithdraw
            ? bookings[bID].state = State.Completed
            : bookings[bID].state = State.UserCanWithdraw;

        address payable user = msg.sender;

        uint256 amount = pendingBookingWithdraws[user][bID];

        if (amount > 0) {
            // Important to set this to 0 in order to avoid double calls
            pendingBookingWithdraws[user][bID] = 0;

            // If something fails, reset amount to give
            if (!user.send(amount)) {
                pendingBookingWithdraws[user][bID] = amount;
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * This function allows the user who booked the apartment to enter the apartment.
     * If the user doesn't checkout, this function has to check if the previous guest is still active and if so
     * it should update the state of the apartment with the new guest and send to the previous guest the funds used as deposit.
     *
     * Conditions: This function must be called by the guest of the apartment.
     *             The booking state must be Confirmed in order to access the apartment.
     *             It should update the apartment.guest atribute with the msg.sender.
     *             It should allow the owner of the apartment to claim the booking money.
     *
     * @param bID   Corresponds to the ID of the booking.
     */
    function checkin(uint256 bID) public {
        require(bookings[bID].user == msg.sender);
        require(bookings[bID].state == State.Confirmed);
        //require(now >= bookings[bID].startDate && now < bookings[bID].endDate);  // ENABLE FOR PROD

        // Once checked in, the owner of the apartment can withdraw the funds.
        bookings[bID].state = State.Withdrawable;

        apartments[bookings[bID].apartmentId].guest = msg.sender;

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been checked in, owner can now withdraw funds and user can view accessCode and withdraw deposit"
        );
    }

    /**
     * This function allows the user who booked the apartment to complete his stay.
     * It should update the apartment.guest atribute with the msg.sender and it should give back the deposit.
     *
     * Conditions: This function must be called by the guest of the apartment and it should
     *             update the state of the booking and the guest of the apartment must be set to 0x address.
     *             In order to use this funtion 'now' should be less or equal to the endDate of the booking
     *
     * @param bID   Corresponds to the ID of the booking
     * @return      true if everything went correctly, false otherwise
     */

    function checkout(uint256 bID) public returns (bool) {
        require(apartments[bookings[bID].apartmentId].guest == msg.sender);
        require(bookings[bID].user == msg.sender);
        require(
            bookings[bID].state == State.Withdrawable ||
                bookings[bID].state == State.UserCanWithdraw
        );
        require(now <= bookings[bID].endDate);

        bookings[bID].state == State.Withdrawable
            ? bookings[bID].state = State.OwnerCanWithdraw
            : bookings[bID].state = State.Completed;

        apartments[bookings[bID].apartmentId].guest = address(0);

        address payable user = msg.sender;

        uint256 amount = pendingDepositWithdraws[user][bID];

        // If the user has pending funds (deposit) the contract will send him automatically the amount once he does the check out
        if (amount > 0) {
            // Important to set this to 0 in order to avoid double calls
            pendingDepositWithdraws[user][bID] = 0;

            // If something fails, reset amount to give
            if (!user.send(amount)) {
                pendingDepositWithdraws[user][bID] = amount;
                return false;
            }
        }

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been completed"
        );

        return true;
    }

    /**
     * This function allows the owner of an apartment to complete a booking that
     * hasn't been neither canceled or checked in by the user who booked. This way the owner
     * can withdraw the funds of a booking that can't be checked and hasn't been canceles by
     * the user. It should update the pendingBookingWithdraws, not send the funds
     *
     * @param bID   Corresponds to the ID of the booking
     * @return      true if everything went correctly, false otherwise
     */
    function checkBookingNotUsed(uint256 bID) public returns (bool) {
        require(apartments[bookings[bID].apartmentId].owner == msg.sender);
        require(
            now > bookings[bID].endDate &&
                bookings[bID].state == State.Confirmed
        );

        // If it verifies the second require, then the state of the booking should update
        bookings[bID].state = State.OwnerCanWithdraw;
        // Set the user pending deposit funds to 0;
        pendingDepositWithdraws[bookings[bID].user][bID] = 0;

        // Give the owner the booking funds + deposit to the owner
        pendingBookingWithdraws[apartments[bookings[bID].apartmentId].owner][
            bID
        ] += bookingDeposit;

        emit BookingStateChanged(
            bookings[bID].apartmentId,
            bID,
            msg.sender,
            bookings[bID].user,
            "Booking has been canceled with the funds lost"
        );
        return true;
    }

    /**
     * The user can cancel his booking with a margin of 10 days. It will set the booking date to 0 as it should allow other to book the same dates
     * Conditions: The user must have a valid booking and the booking cannot be canceled within 10 days prior to the booking. If the user cancels within the 10 days
     * before the booking startDate, then he will lose his deposit but get the rest of the booking price
     *
     * @param bID   Corresponds to the booking ID
     * @return      true if everything went correctly, false otherwise
     */
    function cancelBooking(uint256 bID) public returns (bool) {
        require(bookings[bID].user == msg.sender);

        if (bookings[bID].state == State.Confirmed) {
            if (now < (bookings[bID].startDate - 10 days)) {
                bookings[bID].state = State.UserCanWithdraw;
                bookings[bID].startDate = 0;
                bookings[bID].endDate = 0;

                uint256 cashback = pendingBookingWithdraws[
                    apartments[bookings[bID].apartmentId].owner
                ][bID];
                // set the amount withdrawable by the owner to 0
                pendingBookingWithdraws[
                    apartments[bookings[bID].apartmentId].owner
                ][bID] = 0;

                // Instead of refunding the cash, add it to the withdrawable amount
                pendingDepositWithdraws[msg.sender][bID] += cashback;

                emit BookingStateChanged(
                    bookings[bID].apartmentId,
                    bID,
                    apartments[bookings[bID].apartmentId].owner,
                    bookings[bID].user,
                    "Booking has been canceled without lost"
                );

                return true;
            } else if (
                now > (bookings[bID].startDate - 10 days) &&
                now < bookings[bID].startDate
            ) {
                bookings[bID].state = State.Withdrawable;
                bookings[bID].startDate = 0;
                bookings[bID].endDate = 0;

                // Give the owner the deposit
                uint256 cashback = pendingBookingWithdraws[
                    apartments[bookings[bID].apartmentId].owner
                ][bID];
                pendingBookingWithdraws[
                    apartments[bookings[bID].apartmentId].owner
                ][bID] = bookingDeposit;

                // Instead of refunding the cash, add it to the withdrawable amount
                pendingDepositWithdraws[msg.sender][bID] = cashback;

                emit BookingStateChanged(
                    bookings[bID].apartmentId,
                    bID,
                    apartments[bookings[bID].apartmentId].owner,
                    bookings[bID].user,
                    "Booking has been canceled with deposit lost"
                );
                return true;
            } else if (now > bookings[bID].startDate) {
                bookings[bID].state = State.OwnerCanWithdraw;

                // Set the user pending deposit funds to 0;
                pendingDepositWithdraws[msg.sender][bID] = 0;

                // Give the owner the booking funds + deposit to the owner
                pendingBookingWithdraws[
                    apartments[bookings[bID].apartmentId].owner
                ][bID] += bookingDeposit;

                emit BookingStateChanged(
                    bookings[bID].apartmentId,
                    bID,
                    apartments[bookings[bID].apartmentId].owner,
                    bookings[bID].user,
                    "Booking has been canceled with the funds lost"
                );
                return true;
            }
        } else if (bookings[bID].state == State.Created) {
            // When owner doesn't answer
            bookings[bID].state = State.UserCanWithdraw;
            bookings[bID].startDate = 0;
            bookings[bID].endDate = 0;

            uint256 cashback = pendingBookingWithdraws[
                apartments[bookings[bID].apartmentId].owner
            ][bID];
            // set the amount withdrawable by the owner to 0
            pendingBookingWithdraws[
                apartments[bookings[bID].apartmentId].owner
            ][bID] = 0;

            // Instead of refunding the cash, add it to the withdrawable amount
            pendingDepositWithdraws[msg.sender][bID] += cashback;

            emit BookingStateChanged(
                bookings[bID].apartmentId,
                bID,
                apartments[bookings[bID].apartmentId].owner,
                bookings[bID].user,
                "Booking has been canceled without lost"
            );

            return true;
        }

        return false;
    }

    /**
     * Once the user who owns a valid booking checks in, he should be able to retrieve the access code
     * whenever he wants during his stay.
     *
     * Conditions:  The user must be the current guest of the apartment, otherwise he can't obtain the accessCode
     *
     * @param bID   Corresponds to the booking ID
     * @return      Apartment accessCode
     */
    function viewCode(uint256 bID, address sender)
        public
        view
        returns (string memory)
    {
        require(apartments[bookings[bID].apartmentId].guest == sender);
        return apartments[bookings[bID].apartmentId].accessCode;
    }

    // AUXILIAR FUNCTIONS

    /**
     * Auxiliar function to check if a timestamp is comprehended between other two given.
     *
     * @param target    Represents the timestamp to compare
     * @param bottom    Represents the lowest value to compare with target
     * @param top       Represents the highest value to compare with target
     * @return          true if the value is in between the dates, false otherwise
     */
    function isInBetween(
        uint256 target,
        uint256 bottom,
        uint256 top
    ) private pure returns (bool) {
        return (target >= bottom && target <= top);
    }

    /**
     * Auxiliar function to retrieve the list of bookings of an apartment
     *
     * @param aID   Represents the apartment ID of which we want to obtain the bookings associated
     * @return      Array of bookings of the apartment with the id == aID
     */
    function getBookings(uint256 aID) public view returns (uint256[] memory) {
        uint256[] memory bookingsAux = apartments[aID].bookingIDs;
        return bookingsAux;
    }

    /**
     * Auxiliar function to retrieve data from mapping userApartments
     *
     * @param user  Address of the user
     * @return      Array of apartments ids owned by the user
     */
    function getUserApartments(address user)
        public
        view
        returns (uint256[] memory)
    {
        return userApartments[user];
    }

    /**
     * Auxiliar function to retrieve data from mapping userBookings
     *
     * @param user  Address of the user
     * @return      Array of booking ids owned by the user
     */
    function getUserBookings(address user)
        public
        view
        returns (uint256[] memory)
    {
        return userBookings[user];
    }

    /**
     * Auxiliar function to get owner pending funds to withdraw
     *
     * @param user      The user address
     * @param bookingID The booking ID of which we want to get the funds availables to the owner
     * @return          Returns the withdrawable value of the booking
     */
    function getOwnerPendingWithdraws(address user, uint256 bookingID)
        public
        view
        returns (uint256)
    {
        uint256 value = pendingBookingWithdraws[user][bookingID];
        return value;
    }

    /**
     * Auxiliar function to get user pending funds to withdraw
     *
     * @param user      The user address
     * @param bookingID The booking ID of which we want to get the funds availables to the owner
     * @return          Returns the withdrawable value of the booking
     */
    function getUserPendingWithdraws(address user, uint256 bookingID)
        public
        view
        returns (uint256)
    {
        uint256 value = pendingDepositWithdraws[user][bookingID];
        return value;
    }
}
