import Web3 from "web3";

export const idState = {
  0: "Created",
  1: "Confirmed",
  2: "Withdrawable",
  3: "UserCanWithdraw",
  4: "OwnerCanWithdraw",
  5: "Rejected",
  6: "Completed",
};

export const stateId = {
  Created: "0",
  Confirmed: "1",
  Withdrawable: "2",
  UserCanWithdraw: "3",
  OwnerCanWithdraw: "4",
  Rejected: "5",
  Completed: "6",
};

export const deposit = 6000000000000000;

// Valores que se añaden sobre el timestamp inicial para que se registren correctamente
// tanto el check in como el check out
export const checkinSum = 25200;
export const checkoutSum = 7200;

export const usd_eth_exchange = 2500;

export function getUsdValue(value) {
  return Web3.utils.fromWei(value, "ether") * usd_eth_exchange;
}

export function ethToWei(value) {
  return Web3.utils.toWei(value, "ether");
}

export function weiToEth(value) {
  return Web3.utils.fromWei(value, "ether");
}

export function getUsdEthExchange(value) {
  return value * usd_eth_exchange;
}

export function timestampToDate(timestamp) {
  if (timestamp === "0") return "------";
  let a = new Date(timestamp * 1000);
  let months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  let month = months[a.getMonth()];
  return a.getDate() + " " + month + " " + a.getFullYear();
}

export function checkEntryTime(timestamp, currentTime) {
  return currentTime >= timestamp;
}

export function checkExitTime(timestamp, currentTime) {
  return parseInt(timestamp) <= currentTime;
}

export function daysInBetween(startDateUnix, endDateUnix) {
  return Math.floor((endDateUnix - startDateUnix) / (60 * 60 * 24)) + 1;
}

export function onTime(timestamp) {
  return daysInBetween(Date.now(), timestamp) >= 10;
}
