import React, { useState, useEffect } from "react";
import { DateRangePicker } from "react-dates";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Moment from "moment";
import { extendMoment } from "moment-range";

const moment = extendMoment(Moment);

function Datepicker({ bookingInfo, bookingsToDisable }) {
  const [dateRange, setDateRange] = useState({
    startDate: null,
    endDate: null,
  });
  const [focus, setFocus] = useState(null);

  const { startDate, endDate } = dateRange;

  const handleOnDateChange = (startDate, endDate) => {
    setDateRange(startDate, endDate);
    console.log(startDate, endDate);
  };

  const createBookingInfo = () => {
    bookingInfo(dateRange);
  };

  let dates = bookingsToDisable.map((booking) => [
    // Remove a day in order to block the day of the booking startDate and allow the exit day to be booked
    new Date((booking.startDate - 86400) * 1000),
    new Date((booking.endDate - 86400) * 1000),
  ]);

  console.log(dates);

  // Function provided by Harriet Bicknell
  let isBlocked = (date) => {
    let bookedRanges = [];
    let blocked;
    dates.map((dateRange) => {
      return (bookedRanges = [
        ...bookedRanges,
        moment.range(dateRange[0], dateRange[1]),
      ]);
    });
    blocked = bookedRanges.find((range) => range.contains(date));
    return blocked;
  };

  useEffect(() => {
    if (startDate && endDate) {
      createBookingInfo();
      return () => {
        console.log("cleaning effect");
        setDateRange([null, null]);
      };
    }
  }, [dateRange]);

  return (
    <DateRangePicker
      startDatePlaceholderText="Start"
      startDate={startDate}
      onDatesChange={handleOnDateChange}
      endDatePlaceholderText="End"
      endDate={endDate}
      numberOfMonths={1}
      displayFormat="MMM D"
      showClearDates={true}
      focusedInput={focus}
      onFocusChange={(focus) => setFocus(focus)}
      startDateId="startDate"
      endDateId="endDate"
      minimumNights={1}
      isDayBlocked={isBlocked}
    />
  );
}

export default Datepicker;
