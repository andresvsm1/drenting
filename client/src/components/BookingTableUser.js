import React from "react";
import Table from "react-bootstrap/Table";
import {
  weiToEth,
  timestampToDate,
  idState,
  checkEntryTime,
  checkExitTime,
  onTime,
} from "../utils/auxiliars";
import * as SimpleIcon from "react-icons/si";
import Button from "react-bootstrap/Button";

import "./BookingTableUser.css";

export default function BookingTableUser({
  bookingListFiltered,
  sendUserAction,
}) {
  const handleClick = (id, action) => {
    sendUserAction(id, action);
  };

  // onClick should be a function and not a function call **fixed

  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>#</th>
          <th>Fecha Inicio</th>
          <th>Fecha Fin</th>
          <th>
            Fondos Disponibles
            <SimpleIcon.SiEthereum />
          </th>
          <th>Estado</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {bookingListFiltered.map((booking, key) => {
          return (
            <tr key={key}>
              <th>{booking.id}</th>
              <th>{timestampToDate(booking.startDate)}</th>
              <th>{timestampToDate(booking.endDate)}</th>
              <th>{weiToEth(booking.userPendingFunds)}</th>
              <th>{idState[booking.state]}</th>
              <th>
                {booking.state === "6" || booking.state === "5" ? (
                  "Esta reserva ha finalizado"
                ) : (
                  <div className="grid-container">
                    <div className="grid-item">
                      {booking.state === "1" ? (
                        <Button
                          onClick={function () {
                            checkEntryTime(booking.startDate, Date.now()) &&
                            checkExitTime(booking.startDate, Date.now())
                              ? handleClick(booking.id, "checkin")
                              : alert("Todavía no puedes hacer check in");
                          }}
                          variant="success"
                        >
                          Check in
                        </Button>
                      ) : (
                        <Button
                          onClick={function () {
                            checkEntryTime(booking.id, Date.now())
                              ? handleClick(booking.apartmentId, "view-code")
                              : alert("Todavía no puedes hacer check in");
                          }}
                          variant="success"
                        >
                          View Code
                        </Button>
                      )}
                    </div>
                    <div className="grid-item">
                      <Button
                        onClick={function () {
                          (booking.state === "2" || booking.state === "3") &&
                          checkExitTime(booking.startDate, Date.now())
                            ? handleClick(booking.id, "checkout")
                            : alert("No puedes hacer checkout aun");
                        }}
                        variant="warning"
                      >
                        Check out
                      </Button>
                    </div>
                    <div className="grid-item">
                      <Button
                        onClick={function () {
                          if (booking.state === "0") {
                            window.confirm("Vas a cancelar sin penalizacion")
                              ? handleClick(booking.id, "cancel-on-time")
                              : console.log("Se ha cancelado la accion");
                          } else if (booking.state === "1") {
                            if (onTime(booking.startDate)) {
                              window.confirm("Vas a cancelar sin penalizacion")
                                ? handleClick(booking.id, "cancel-on-time")
                                : console.log("Se ha cancelado la accion");
                            } else {
                              window.confirm("Vas a cancelar con penalizacion")
                                ? handleClick(booking.id, "cancel-off-time")
                                : console.log("Se ha cancelado la accion");
                            }
                          } else {
                            alert("No se puede cancelar la reserva");
                          }
                        }}
                        variant="danger"
                      >
                        Cancelar
                      </Button>
                    </div>
                    <div className="grid-item">
                      <Button
                        onClick={function () {
                          (booking.state === "2" ||
                            booking.state === "3" ||
                            booking.state === "5") &&
                          weiToEth(booking.userPendingFunds)
                            ? handleClick(booking.id, "withdraw-user")
                            : alert(
                                "No puedes reclamar los fondos de esta reserva aun"
                              );
                        }}
                        variant="light"
                      >
                        Withdraw
                      </Button>
                    </div>
                  </div>
                )}
              </th>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}
