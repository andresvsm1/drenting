import React from 'react';
import './Loader.css';

export default function Loader() {

    return (
        <>
            <div className="my-5 text-center">
                <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        </>
    );
}