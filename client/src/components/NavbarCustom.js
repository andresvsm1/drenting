import React from "react";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import "./NavbarCustom.css";

import * as BsIcons from "react-icons/bs";

export default function NavbarCustom({ isConnected, onConnectClick }) {
  let currentAccount = isConnected;

  let dropdown = (
    <NavDropdown title={currentAccount} id="collasible-nav-dropdown">
      <Link to="/new-apartment" class="dropdown-item">
        Añadir Vivienda
      </Link>
      <Link to="/my-apartments" class="dropdown-item">
        Mis espacios
      </Link>
      <Link to="/bookings" class="dropdown-item">
        Mis reservas
      </Link>
    </NavDropdown>
  );

  let loginBtn = (
    <Button variant="light" onClick={onConnectClick}>
      Conectar Wallet
    </Button>
  );

  let content;
  if (typeof window.ethereum !== "undefined") {
    content = (
      <Nav className="ml-auto">
        <Link to="/" class="nav-link">
          Home
        </Link>
        <Link to="/apartments" class="nav-link">
          Viviendas
        </Link>
        {currentAccount !== undefined ? dropdown : loginBtn}
      </Nav>
    );
  } else {
    content = (
      <Nav className="ml-auto">
        <Link to="/" class="nav-link">
          Home
        </Link>
        <Link to="/about-us" class="nav-link">
          Mas informacion
        </Link>
      </Nav>
    );
  }

  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand>
          <Link to="/" className="navbar-brand">
            <BsIcons.BsHouseFill className="logo" />
            DRenting
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">{content}</Navbar.Collapse>
      </Navbar>
    </>
  );
}
