import React from "react";
import casa from "../images/casa.png";
import { Link, useParams } from "react-router-dom";
import { BsCalendar } from "react-icons/bs";
import { timestampToDate } from "../utils/auxiliars";
import { Button } from "react-bootstrap";

import BookingTableUser from "../components/BookingTableUser";
import PageNotFound from "./PageNotFound";

export default function BookingView({
  userAccount,
  apartmentsList,
  bookingList,
  handleAction,
}) {
  const { apartmentId } = useParams();
  const apartment = apartmentsList[apartmentId];
  // Filter bookings by apartment and by user
  function filterBookingsByApartment() {
    try {
      return bookingList.filter(
        (booking) =>
          booking.apartmentId === apartmentId &&
          booking.user.toLowerCase() === userAccount.toLowerCase()
      );
    } catch (error) {
      return [];
    }
  }

  let bookingListUser = filterBookingsByApartment();
  // Controlar si la ruta es valida o no
  let content;
  try {
    if (bookingListUser.length > 0) {
      content = (
        <div className="main-container">
          <div className="ApartmentPage">
            <div className="ApartmentBanner">
              <div className="Container -Container--full">
                <div className="ApartmentBanner-container">
                  <div className="ApartmentImage">
                    <div className="ApartmentImage-main">
                      <div className="ApartmentImage-main-container">
                        <img
                          className="Apartment-image"
                          src={casa}
                          alt="imagen demo"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="ApartmentPage-content">
              <div className="ApartmentProfile">
                <div className="Container Container--smGrow">
                  <div className="ApartmentSection">
                    <div className="ApartmentSection-content">
                      <div className="ApartmentHeader">
                        <div className="ApartmentHeader-main">
                          <div className="ApartmentHeader-title">
                            <h1 className="ApartmentHeader-title-text">
                              {apartment.title}
                            </h1>
                          </div>
                          <div className="ApartmentHeader-details">
                            <span className="ApartmentHeader-details-item">
                              #{apartment.id}
                            </span>
                          </div>
                        </div>
                        <div className="ApartmentHeader-owner">
                          <span class="ApartmentHeader-owner-details">
                            <span class="ApartmentHeader-owner-name">
                              {apartment.owner}
                            </span>
                            <span class="ApartmentHeader-owner-label">
                              Propietario
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="ApartmentSection">
                    <div className="ApartmentSection-header">
                      <h2 className="ApartmentSection-header-title">
                        Descripcion
                      </h2>
                    </div>
                    <div className="ApartmentSection-content">
                      <p>{apartment.description}</p>
                      <p>Ubicación: {apartment.direction}</p>
                      <div className="ApartmentInfo-uploadInfo">
                        <div className="ApartmentInfo-uploadInfo-list">
                          <BsCalendar className="ApartmentInfo-calendar-icon" />
                          <span>
                            <div className="ApartmentInfo-uploadInfo-title">
                              Publicada
                            </div>
                            <h3>
                              {apartment.creationDate
                                ? timestampToDate(apartment.creationDate)
                                : "No date to display"}
                            </h3>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="ApartmentSection">
                    <div className="ApartmentSection-header">
                      <h2 className="ApartmentSection-header-title">
                        Reservas
                      </h2>
                      <div className="ApartmentHeader-details">
                        <span className="ApartmentHeader-details-item">
                          Aqui puedes ver las reservas que tienes en este
                          apartamento. Cuando llegue el día de tu reserva podrás
                          hacer obtener la clave de acceso a la vivienda.
                          <br />
                          Si quieres cancelar alguna reserva, tienes la opcion
                          de hacerlo sin penalizacion siempre que lo hagas con
                          un margen de 10 dias. En caso contrario, perderas el
                          deposito realizado e incluso puede que pierdas el
                          valor total de la reserva.
                        </span>
                      </div>
                    </div>
                    <div className="ApartmentSection-content">
                      <BookingTableUser
                        bookingListFiltered={bookingListUser}
                        sendUserAction={(id, action) => {
                          handleAction(id, action);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      content = (
        <div className="my-5 text-center">
          <h1>Algo ha ido mal</h1>
          <p className="regular-description">Vuelve al inicio</p>
          <Button variant="dark">
            <Link to="/">Home</Link>
          </Button>
        </div>
      );
    }
  } catch (error) {
    content = <PageNotFound />;
  }

  return <>{content}</>;
}
