import React from "react";
import "./Apartments.css";
import casa from "../images/casa.png";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import "../App.css";

export default function Apartments({ apartmentsList, userAccount }) {
  let content, apartmentsFiltered;
  apartmentsFiltered = apartmentsList.filter(
    (apartment) => apartment.available === true
  );

  if (apartmentsFiltered.length !== 0) {
    content = (
      <div className="main-container">
        <div className="upper-container">
          <div className="apartments-content">
            <h1 className="header">Encuentra tu alojamiento</h1>
            <p className="header-subtitle">
              Estas son las apartments actualmente disponibles
            </p>
            <div className="container">
              <div class="row">
                {apartmentsFiltered.map((apartment, key) => {
                  return (
                    <div key={key}>
                      <div class="col">
                        <Card style={{ width: "21rem", marginBottom: "2rem" }}>
                          <Card.Img variant="top" src={casa} />
                          <Card.Body>
                            <Card.Title>{apartment.title}</Card.Title>
                            <Card.Text
                              style={{
                                width: "200px",
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                              }}
                            >
                              {apartment.description}
                            </Card.Text>
                            <Button variant="dark">
                              <Link to={`/apartments/${apartment.id}`}>
                                Ver más
                              </Link>
                            </Button>
                          </Card.Body>
                        </Card>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    content = (
      <div className="my-5 text-center">
        <h1>No existen viviendas actualmente</h1>
        <p className="regular-description">
          ¡Puedes añadir una vivienda que quieras alquilar!
        </p>
        {userAccount ? (
          <Button variant="dark">
            <Link to="/new-apartment">Añade una vivienda</Link>
          </Button>
        ) : (
          <p className="regular-description">
            Conecta tu wallet desde la barra de navegación para poder añadir una
          </p>
        )}
      </div>
    );
  }

  return <>{content}</>;
}
