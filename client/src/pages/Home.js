import React from "react";
import "../App.css";

export default function Home({ apartmentsList }) {
  return (
    <>
      <div className="my-5 text-center">
        <h1>¿Qué es D-Renting?</h1>
        <p className="regular-description">
          D-Renting es un prototipo que intenta simular el funcionamiento de la
          conocida plataforma AirBnb de reservas de alojamientos pero con una
          particularidad, y es que toda la magia de la aplicación sucede bajo el
          control de un contrato inteligente. La principal ventaja es la
          privacidad del usuario, no necesitas registrarte en ningun lado ni
          ceder tus datos a nadie, simplemente conectas tu cartera digital e
          interactuas con la aplicación.
        </p>
      </div>
    </>
  );
}
