import React from "react";
import "./ViewApartment.css";
import casa from "../images/casa.png";
import Button from "react-bootstrap/Button";
import { Link, useParams } from "react-router-dom";
import { BsCalendar } from "react-icons/bs";
import Datepicker from "../components/Datepicker";
import { getUsdValue, timestampToDate, stateId } from "../utils/auxiliars";
import Web3 from "web3";
import PageNotFound from "./PageNotFound";
import moment from "moment";

export default function ViewApartment({
  bookingList,
  apartmentsList,
  onCreateBooking,
  userAccount,
}) {
  const { apartmentId } = useParams();
  const apartment = apartmentsList[apartmentId];

  let startDateUnix;
  let endDateUnix;

  const datesToUnix = (dateRanges) => {
    let { startDate, endDate } = dateRanges;
    startDate = moment(startDate).toDate();
    endDate = moment(endDate).toDate();
    startDate.setHours(17, 0, 0);
    endDate.setHours(12, 0, 0);

    startDateUnix = moment(startDate).unix();
    endDateUnix = moment(endDate).unix();
  };

  function bookingsFilteredByApartment() {
    try {
      return bookingList.filter(
        (booking) =>
          booking.apartmentId === apartmentId &&
          booking.state !== stateId.Created
      );
    } catch (error) {
      return [];
    }
  }

  const bookingsFiltered = bookingsFilteredByApartment();

  // Send info for a new booking
  const sendBookingInfo = () => {
    // If dates exist continue
    try {
      if (startDateUnix && endDateUnix && apartment.id) {
        onCreateBooking(apartment.id, startDateUnix, endDateUnix);
      } else {
        alert("Necesitas seleccionar las fechas de la reserva");
        console.log("Some value is missing");
      }
    } catch (error) {
      console.log(error);
    }
  };
  let content, control;
  try {
    // Controlar si la ruta es valida o no
    if (
      userAccount &&
      userAccount.toLowerCase() === apartment.owner.toLowerCase()
    ) {
      control = (
        <div className="ApartmentRent-action">
          <Button variant="dark">
            <Link to={`/my-apartments/${apartment.id}`}>Adminitrar</Link>
          </Button>
        </div>
      );
    } else {
      control = (
        <div className="ApartmentRent-action">
          <div className="datepickerContainer">
            <Datepicker
              bookingsToDisable={bookingsFiltered}
              bookingInfo={(dateRange) => datesToUnix(dateRange)}
            />
          </div>
          <Button
            variant="dark"
            onClick={
              userAccount
                ? sendBookingInfo
                : function () {
                    alert("Necesitas conectar tu wallet para reservar");
                  }
            }
          >
            Alquilar con ETH
          </Button>
          ;
        </div>
      );
    }
    content = (
      <div className="main-container">
        <div className="ApartmentPage">
          <div className="ApartmentBanner">
            <div className="Container -Container--full">
              <div className="ApartmentBanner-container">
                <div className="ApartmentImage">
                  <div className="ApartmentImage-main">
                    <div className="ApartmentImage-main-container">
                      <img
                        className="Apartment-image"
                        src={casa}
                        alt="imagen demo"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="ApartmentPage-content">
            <div className="ApartmentProfile">
              <div className="Container Container--smGrow">
                <div className="ApartmentSection">
                  <div className="ApartmentSection-content">
                    <div className="ApartmentHeader">
                      <div className="ApartmentHeader-main">
                        <div className="ApartmentHeader-title">
                          <h1 className="ApartmentHeader-title-text">
                            {apartment.title}
                          </h1>
                        </div>
                        <div className="ApartmentHeader-details">
                          <span className="ApartmentHeader-details-item">
                            #{apartment.id}
                          </span>
                        </div>
                      </div>
                      <div className="ApartmentHeader-owner">
                        <span class="ApartmentHeader-owner-details">
                          <span class="ApartmentHeader-owner-name">
                            {apartment.owner}
                          </span>

                          <span class="ApartmentHeader-owner-label">
                            Propietario
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="ApartmentSection">
                  <div className="ApartmentSection-content">
                    <div className="ApartmentRent ApartmentRent--sale">
                      <div className="ApartmentRent-boxes">
                        <div className="ApartmentRent-box">
                          <h3 className="ApartmentRent-box-title">Alquilar</h3>
                          <span className="ApartmentRent-box-subtitle">
                            {Web3.utils.fromWei(apartment.price, "ether")}
                            <small>ETH/Noche</small>
                            <small className="ApartmentRent-box-currency-price">
                              ${getUsdValue(apartment.price)} USD
                            </small>
                          </span>
                        </div>
                        {control}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="ApartmentSection">
                  <div className="ApartmentSection-header">
                    <h2 className="ApartmentSection-header-title">
                      Descripcion
                    </h2>
                  </div>
                  <div className="ApartmentSection-content">
                    <p>{apartment.description}</p>
                    <p>Ubicación: {apartment.direction}</p>
                    <div className="ApartmentInfo-uploadInfo">
                      <div className="ApartmentInfo-uploadInfo-list">
                        <BsCalendar className="ApartmentInfo-calendar-icon" />
                        <span>
                          <div className="ApartmentInfo-uploadInfo-title">
                            Publicada
                          </div>
                          <h3>
                            {apartment.creationDate
                              ? timestampToDate(apartment.creationDate)
                              : "No date to display"}
                          </h3>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } catch (error) {
    content = <PageNotFound />;
  }

  return <>{content}</>;
}
