import React from "react";
import casa from "../images/casa.png";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export default function UserBookings({ apartmentList, bookingsFiltered }) {
  let apartmentsIDs = bookingsFiltered.map((booking) => booking.apartmentId);
  let apartmentsToDisplay = [...new Set(apartmentsIDs)];

  let apartmetsFiltered = apartmentList.filter((apartment) =>
    apartmentsToDisplay.includes(apartment.id)
  );

  let content;
  if (bookingsFiltered.length > 0) {
    content = (
      <div className="main-container">
        <div className="upper-container">
          <div className="apartments-content">
            <h1 className="header">
              Estas son las viviendas que tienes reservadas
            </h1>
            <p className="header-subtitle">
              ¡Puedes consultar el estado de tus reservas y gestionarlas!
            </p>
            <div className="container">
              <div class="row">
                {apartmetsFiltered.map((apartment, key) => {
                  return (
                    <div key={key}>
                      <div class="col-sm-6">
                        <Card style={{ width: "18rem" }}>
                          <Card.Img variant="top" src={casa} />
                          <Card.Body>
                            <Card.Title>{apartment.title}</Card.Title>
                            <Card.Text>{apartment.description}</Card.Text>
                            <Button variant="dark">
                              <Link to={`/bookings/${apartment.id}`}>
                                Ver más
                              </Link>
                            </Button>
                          </Card.Body>
                        </Card>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    content = (
      <div className="my-5 text-center">
        <h1>Actualmente no tienes reservas</h1>
        <p className="regular-description">
          Puedes reservar alguna de las viviendas disponibles.
        </p>
        <Button variant="dark">
          <Link to="/apartments">Reservar</Link>
        </Button>
      </div>
    );
  }

  return <>{content}</>;
}
