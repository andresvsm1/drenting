import React from 'react';
import './PageNotFound.css'

export default function PageNotFound() {

    return (
        <>
            <div className="my-5 text-center">
                <h1> Routing error</h1>
                <p className="error-description">
                    No se ha encontrado la pagina que has buscado.
                </p>
            </div>
        </>
    )
}