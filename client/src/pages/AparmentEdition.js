import React, { useState } from "react";
import { useParams } from "react-router-dom";

import "./NewApartment.css";
import { getUsdEthExchange, ethToWei, weiToEth } from "../utils/auxiliars";
import casa from "../images/casa.png";

export default function ApartmentEdition({ apartmentsList, onEditApartment }) {
  const { apartmentId } = useParams();

  const handleInputChange = (event) => {
    setdata({
      ...data,
      [event.target.name]: event.target.value,
    });
  };

  const apartment = apartmentsList[apartmentId];

  const [data, setdata] = useState({
    title: "",
    description: "",
    direction: "",
    price: 0,
    codigo: 0,
  });

  const enviardata = (event) => {
    event.preventDefault();
    console.log(
      "Editando apartmento...\nTitulo: " +
        data.title +
        "\nDescripcion: " +
        data.description +
        "\nDireccion " +
        data.direction +
        "\nPrecio/dia " +
        data.price +
        "\nCodigo: " +
        data.code
    );

    // Send value as wei
    let weiValue = ethToWei(data.price);
    onEditApartment(
      apartmentId,
      data.title,
      data.description,
      data.direction,
      weiValue,
      data.code
    );
  };

  return (
    <>
      <div className="container">
        <div className="box">
          <div className="upperbox">
            <div className="circle"></div>
            <img src={casa} alt="icono" />
          </div>
          <div className="data">
            <h1 className="add-apartment">Edita los campos de tu vivienda</h1>
            <h3>
              Se muestran los antiguos valores, pero debes introducirlos
              nuevamente en caso de no querer modificarlos
            </h3>
            <form className="column" onSubmit={enviardata}>
              <div className="field">
                <input
                  type="text"
                  placeholder={apartment.title}
                  className="form-control"
                  onChange={handleInputChange}
                  name="title"
                  required
                ></input>
              </div>
              <div className="field">
                <textarea
                  type="text"
                  placeholder={apartment.description}
                  className="form-control"
                  onChange={handleInputChange}
                  name="description"
                  required
                ></textarea>
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder={apartment.direction}
                  className="form-control"
                  onChange={handleInputChange}
                  name="direction"
                  required
                ></input>
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder={weiToEth(apartment.price)}
                  className="form-control"
                  onChange={handleInputChange}
                  name="price"
                  required
                ></input>
                <div>
                  <p className="usd-rate" id="usd-rate">
                    USD value: {data.price ? getUsdEthExchange(data.price) : 0}$
                  </p>
                </div>
              </div>
              <div className="field">
                <input
                  type="text"
                  minLength="6"
                  maxLength="6"
                  placeholder={apartment.accessCode}
                  className="form-control"
                  onChange={handleInputChange}
                  name="code"
                  required
                ></input>
              </div>
              <button type="submit" className="btn-add">
                Guardar Cambios
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
